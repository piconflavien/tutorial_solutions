namespace cpp Ex2

struct Vec3
{
    1: double x,
    2: double y,
    3: double z
}

service AddTwoVectors {
   Vec3 add(1:Vec3 vec1, 2:Vec3 vec2)
}
