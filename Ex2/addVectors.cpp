#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TTransportUtils.h>

#include "gen-cpp/AddTwoVectors.h"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

using namespace Ex2;

class AddTwoVectorsHandler : virtual public AddTwoVectorsIf {
public:
  AddTwoVectorsHandler () {}

  void add(Vec3& sum, const Vec3& v1, const Vec3& v2) {
    std::cerr << "add([" << v1.x << ", " << v1.y << ", " << v1.z << "], ";
    std::cerr << "[" << v2.x << ", " << v2.y << ", " << v2.z << "])\n";
    // Vec3 sum;
    sum.x = v1.x + v2.x;
    sum.y = v1.y + v2.y;
    sum.z = v1.z + v2.z;
    // return sum;
  }
};

int main(int argc, char **argv) {
	 
#ifdef WIN32
	/** Init windows sockets :D **/
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0)
    {
            /* Tell the user that we could not find a usable */
            /* Winsock DLL.                                  */
            printf("WSAStartup failed with error: %d\n", err);

            // TODO Throw exception??
    }
    /** End init windows socket**/
#endif

  boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
  boost::shared_ptr<AddTwoVectorsHandler> handler(new AddTwoVectorsHandler());
  boost::shared_ptr<TProcessor> processor(new AddTwoVectorsProcessor(handler));
  boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(9090));
  boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());

  TSimpleServer server(processor,
                       serverTransport,
                       transportFactory,
                       protocolFactory);
  printf("Starting the server...\n");
  server.serve();
  printf("done.\n");
  return 0;
}
