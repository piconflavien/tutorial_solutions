#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>

#include "gen-cpp/VectorOperations.h"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using namespace Ex3;

int main(int argc, char** argv) {
  boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9090));
  boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
  boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

  VectorOperationsClient client(protocol);

  try {
    transport->open();
    Vec3 v1, v2;
    v1.x = 3; v1.y = 1, v1.z = 10;
    v2.x = 2; v2.y = -1, v2.z = 2;
    Vec3 sum;
    client.add(sum, v1, v2);
    std::cerr << "add([" << v1.x << ", " << v1.y << ", " << v1.z << "], ";
    std::cerr << "[" << v2.x << ", " << v2.y << ", " << v2.z << "])";
    std::cerr << " = [" << sum.x << ", " << sum.y << ", " << sum.z << "]\n";
    double dotPrd =  client.dot(v1, v2);
    std::cerr << "dot([" << v1.x << ", " << v1.y << ", " << v1.z << "], ";
    std::cerr << "[" << v2.x << ", " << v2.y << ", " << v2.z << "])";
    std::cerr << " = " << dotPrd << "\n";
    transport->close();
  } catch (TException &tx) {
    printf("ERROR: %s\n", tx.what());
  }
}



