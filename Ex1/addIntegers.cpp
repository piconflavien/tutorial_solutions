#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TTransportUtils.h>

#include "gen-cpp/AddTwoIntegers.h"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;

using namespace Ex1;

class AddTwoIntegersHandler : public AddTwoIntegersIf {
public:
  AddTwoIntegersHandler () {}

  int32_t add(const string& userName, const int32_t n1, const int32_t n2) {
    std::cerr << "Received request from " << userName << " add(" << n1 << ", " << n2 << ")\n";
    return n1 + n2;
  }
};

int main(int argc, char **argv) {

#ifdef WIN32
	/** Init windows sockets :D **/
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0)
    {
            /* Tell the user that we could not find a usable */
            /* Winsock DLL.                                  */
            printf("WSAStartup failed with error: %d\n", err);

            // TODO Throw exception??
    }
    /** End init windows socket**/
#endif

  boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
  boost::shared_ptr<AddTwoIntegersHandler> handler(new AddTwoIntegersHandler());
  boost::shared_ptr<TProcessor> processor(new AddTwoIntegersProcessor(handler));
  boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(9090));
  boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());

  TSimpleServer server(processor,
                       serverTransport,
                       transportFactory,
                       protocolFactory);
  printf("Starting the server...\n");
  server.serve();
  printf("done.\n");
  return 0;
}
